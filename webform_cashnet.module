<?php
/**
 * @file
 *
 */

/**
 * Implements hook_permission().
 */
function webform_cashnet_permission() {
  return array(
    'edit webform cashnet component' => array(
      'title' => t('Add or edit Webform CASHNet components.'),
      'description' => t('This permission allows users to configure forms to submit to CASHNet.'),
    ),
  );
}

/**
 * Implements hook_webform_component_info().
 */
function webform_cashnet_webform_component_info() {
  $components = array();
  $components['cashnet_item'] = array(
    'label' => t('CASHNet Item'),
    'description' => t('Configure a form to submit an item to CASHNet.'),
    'features' => array(
      'csv' => TRUE,
      'default_value' => FALSE,
      'description' => FALSE,
      'email' => TRUE,
      'email_address' => FALSE,
      'email_name' => FALSE,
      'required' => FALSE,
      // Need title => TRUE to get $form['name'] on the edit form, which is
      // necessary to avoid "undefined index" errors.
      'title' => TRUE,
      'title_display' => FALSE,
      'title_inline' => FALSE,
      'conditional' => FALSE,
      'spam_analysis' => FALSE,
      'group' => FALSE,
      'attachment' => FALSE,
      // If a CASHNet Item is marked "Private," then unless Anonymous Users have
      // result access permissions (which they shouldn't), the CASHNet Item will
      // fail to generate a value (commonly used as the order number). The item
      // *will* still submit, though, which is good!
      'private' => FALSE,
    ),
    // 'file' => 'components/cashnet_item.inc',
  );

  return $components;
}

/**
 * Implements _webform_defaults_COMPONENT().
 */
function _webform_defaults_cashnet_item() {
  return array(
    'name' => 'cashnet item',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      // Need to specify this default value to prevent "undefined index" errors.
      'private' => 0,
      // 'width' => '',
      // 'unique' => 0,
      // 'disabled' => 0,
      // 'title_display' => 0,
      // 'description' => '',
      'mappings' => array(),
      'item_code' => '',
      'control_field_type' => '',
      'control_field_name' => '',
      'control_field_value' => '',
    ),
  );
}

/**
 * Implements _webform_submit_COMPONENT().
 */
function _webform_submit_cashnet_item($component, $value) {
  // Preserve existing ordernumber values (necessary if an existing submission
  // is re-edited).  If no existing order number, generate a new one.
  if (empty($value)) {
    $component = json_encode($component);
    $post = json_encode($_POST);
    srand();
    $rand = rand();
    if (function_exists('mt_rand')) {
      mt_srand();
      $rand .= mt_rand();
    }
    $value = substr(hash('sha256', microtime() . $rand . uniqid('order_number_generator', TRUE) . ip_address() . $post . $component), 0, 16);
  }
  return $value;
}

/**
 * Implements _webform_render_COMPONENT().
 */
function _webform_render_cashnet_item($component, $value = NULL, $filter = TRUE) {
  // Return a dummy, empty item, sufficient to convince the Webform module that
  // this component is actually "able to be rendered" (though nothing shows up).
  $component_value = '';
  // If this component already has a submitted value, for example on a
  // submission edit form, retain the old value.
  if (!empty($value) && !empty($value[0])) {
    $component_value = $value[0];
  }
  return array(
    '#type' => 'value',
    '#value' => $component_value,
  );
}

/**
 * Implements _webform_display_COMPONENT().
 */
function _webform_display_cashnet_item($component, $value, $format = 'html') {
  $element = array(
    '#title' => $component['name'],
    '#markup' => isset($value[0]) ? $value[0] : NULL,
    '#weight' => $component['weight'],
    '#format' => $format,
    '#theme' => 'webform_display_hidden',
    '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
    '#translatable' => array('title'),
  );

  return $element;
}

/**
 * Implements _webform_csv_headers_COMPONENT().
 */
function _webform_csv_headers_cashnet_item($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_COMPONENT().
 */
function _webform_csv_data_cashnet_item($component, $export_options, $value) {
  return empty($value[0]) ? '' : $value[0];
}

/**
 * Implements _webform_table_COMPONENT().
 */
function _webform_table_cashnet_item($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_edit_COMPONENT().
 */
function _webform_edit_cashnet_item($component) {
  if (!user_access('edit webform cashnet component')) {
    // Enforce access restrictions here - can't return MENU_ACCESS_DENIED as it
    // won't bubble up to menu_execute_active_handler().
    drupal_access_denied();
    return;
  }
  $node = node_load($component['nid']);
  $form = array();
  $options = array('' => t('None'));
  $items = _webform_cashnet_get_items();
  foreach ($items as $item_code => $item) {
    $options[$item_code] = $item['name'];
  }

  $form['extra'] = array('#tree' => TRUE);
  $form['extra']['item_code'] = array(
    '#title' => t('CASHNet Item'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $component['extra']['item_code'],
    '#weight' => 10,
    '#description' => t('Choose the corresponding CASHNet Item code/configuration for this form.'),
    '#ajax' => array(
      'callback' => 'webform_cashnet_ajax_edit_cashnet_item_item_code',
      'wrapper' => 'mappings-ajax-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['control_fieldset'] = array(
    '#title' => t('CASHNet Submission Control'),
    '#type' => 'fieldset',
    '#weight' => 15,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Control when this item is submitted to CASHNet.'),
  );

  $form['control_fieldset']['control_field_type'] = array(
    '#title' => t('Control Type'),
    '#type' => 'radios',
    '#options' => array(
      '' => t('None: Always submit this item (when required fields are set)'),
      'field_empty' => t('Control field empty: Submit when the control field is empty'),
      'field_notempty' => t('Control field NOT empty: Submit when the control field is NOT empty'),
      'field_value' => t('Control field is value: Submit when the control field matches a specified value'),
      'field_notvalue' => t('Control field is NOT value: Submit when the control field DOES NOT match any specified value'),
    ),
    '#default_value' => $component['extra']['control_field_type'],
    '#weight' => 10,
    '#description' => t('Choose the type of control to exercise for this item.'),
    '#parents' => array('extra', 'control_field_type'),
  );

  $options = array('' => 'None');
  foreach ($node->webform['components'] as $webform_cid => $webform_component) {
    if ($webform_component['type'] != 'cashnet_item') {
      // Use "CSV" to determine whether or not this component can be used for
      // CASHNet (kind of similar - submitting to a flat, text-only 3rd party).
      if (webform_component_feature($webform_component['type'], 'csv')) {
        $options[$webform_cid] = $webform_component['name'];
      }
    }
  }

  $form['control_fieldset']['control_field_name'] = array(
    '#title' => t('Control Field'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $component['extra']['control_field_name'],
    '#weight' => 20,
    '#description' => t('Choose the field to control submissions.'),
    '#states' => array(
      // 'invisible' => array(
        // ':input[name="extra[control_fieldset][control_field_type]"]' => array('value' => ''),
      // ),
      // 'required' => array(
        // ':input[name="vanity_warning_accept"]' => array('visible' => TRUE),
      // ),
    ),
    '#parents' => array('extra', 'control_field_name'),
  );

  $form['control_fieldset']['control_field_value'] = array(
    '#title' => t('Control Field Value(s)'),
    '#type' => 'textarea',
    '#default_value' => $component['extra']['control_field_value'],
    '#weight' => 30,
    '#description' => t('List of field values, one per line, to control submissions.'),
    '#states' => array(
      // 'invisible' => array(
        // ':input[name="extra[control_fieldset][control_field_type]"]' => array('value' => ''),
      // ),
      // 'required' => array(
        // ':input[name="vanity_warning_accept"]' => array('visible' => TRUE),
      // ),
    ),
    '#parents' => array('extra', 'control_field_value'),
  );

  $form['mappings_fieldset'] = array(
    '#tree' => TRUE,
    '#title' => t('Item Value Mappings'),
    '#type' => 'fieldset',
    '#weight' => 20,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t("Choose the form inputs that correspond to this item's values (as submitted to CASHNet). Common- and ref-type item values are specified per-item. Submission-type values take effect across the entire submission, even if multiple items of different types are submitted."),
  );

  $form['mappings_fieldset']['mappings'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="mappings-ajax-wrapper">',
    '#suffix' => '</div>',
    '#parents' => array('extra', 'mappings'),
  );

  $form['component_extra'] = array(
    '#type' => 'value',
    '#value' => $component['extra'],
  );

  return $form;
}

/**
 * Internal-use function to return per-submission values.
 *
 * Only one copy of these values will be sent per submission, regardless of the
 * number of submitting CASHNet Items.
 */
function _webform_cashnet_per_submission_values() {
  return array(
    'custcode' => FALSE,
    'fname' => FALSE,
    'lname' => FALSE,
    'addr' => FALSE,
    'city' => FALSE,
    'state' => FALSE,
    'zip' => FALSE,
    'signouturl' => FALSE,
  );
}

/**
 * Internal-use function to return per-submission, optional reference values.
 *
 * Each item in CASHNet Checkout can define free-form "reference values." These
 * are NOT guaranteed to exist in the CASHNet store, but if they are defined,
 * then they should only be submitted once per submission. See the CASHNet
 * ePayment eMarket Manual's "Checkout Data Elements" table for more info.
 */
function _webform_cashnet_per_submission_reference_values() {
  $refvals = &drupal_static(__FUNCTION__, NULL);
  if (!isset($refvals)) {
    $refvals = array(
      'ADDR_G',
      'NAME_G',
      'CITY_G',
      'STATE_G',
      'ZIP_G',
      'COUNTRY_G',
      'EMAIL_G',
    );
    $module_defined = module_invoke_all('webform_cashnet_per_submission_reference_values');
    if (!isset($module_defined) || !is_array($module_defined)) {
      $module_defined = array();
    }
    foreach ($module_defined as $refval) {
      $refvals[] = strtoupper(trim($refval));
    }
    drupal_alter('webform_cashnet_per_submission_reference_values', $refvals);
    $refvals = array_combine(array_values($refvals), array_fill(0, count($refvals), FALSE));
  }
  return $refvals;
}

/**
 * Internal-use function to gather mappings that apply to a given CASHNet item.
 */
function _webform_cashnet_get_items($item_code = FALSE, $store_code = FALSE) {
  $mappings = &drupal_static('webform_cashnet_mappings', FALSE);
  if ($mappings === FALSE) {
    $items = module_invoke_all('webform_cashnet_items');
    drupal_alter('webform_cashnet_items', $items);
    $mappings = array(
      'items' => array(),
      'stores' => array(),
    );
    // Compile the 'stores' array and fix all $items w/default values.
    foreach ($items as $code => $item) {
      // Ensure all the necessary array members exist.
      $item = ((array) $item) + array(
        'itemcode' => $code,
        'name' => FALSE,
        'desc' => FALSE,
        'store' => FALSE,
        'amount' => FALSE,
        'gl' => FALSE,
        'qty' => FALSE,
        'refs' => array(),
      );
      // The array's key, $code, may be destroyed by Drupal if it is numeric
      // (even if it is stored in a string, e.g. '123'), so use the item's
      // "itemcode" value.
      if (is_numeric($code)) {
        $code = $item['itemcode'];
      }
      $item['refs'] = !empty($item['refs']) ? (array) $item['refs'] : array();
      $item['submission'] = !empty($items['submission']) ? (array) $item['submission'] : array();
      $item['submission'] += _webform_cashnet_per_submission_values();
      // Skip this item if required info is missing.
      if (empty($item['name']) || empty($item['store']) || empty($code)) {
        continue;
      }
      $store = $item['store'];
      if (!isset($mappings['stores'][$store])) {
        $mappings['stores'][$store] = array();
      }
      // Store by key to auto-dedupe item codes.
      $mappings['stores'][$store][$code] = TRUE;
      $mappings['items'][$code] = $item;
    }
    // Collapse 'stores' items into flat arrays.
    foreach ($mappings['stores'] as &$store_items) {
      $store_items = array_keys($store_items);
    }
  }
  if (!empty($item_code)) {
    return !empty($mappings['items'][$item_code]) ? array($item_code => $mappings['items'][$item_code]) : FALSE;
  }
  else if (!empty($store_code)) {
    return !empty($mappings['stores'][$store_code]) ? array($store_code => $mappings['stores'][$store_code]) : FALSE;
  }
  return $mappings['items'];
}

/**
 * Internal-use function to turn an item into form fields on the component form.
 */
function _webform_cashnet_get_item_form_fields($item, $form, $form_state) {
  $node = node_load($form['nid']['#value']);
  $form_components = array();
  foreach ($node->webform['components'] as $component) {
    // Use "CSV" to determine whether or not this component can be used for
    // CASHNet (kind of similar - submitting to a flat, text-only 3rd party).
    if (webform_component_feature($component['type'], 'csv')) {
      // Choices in #options cannot be escaped: doing so leads to double-
      // escaping.
      // @see form_select_options()
      $form_components['component: ' . $component['cid']] = t('Component: !component', array('!component' => $component['name']));
    }
  }
  $option_item_template = array(
    '#title' => '',
    '#type' => 'select',
    '#options' => array('' => 'No value'),
    '#default_value' => FALSE,
    '#description' => FALSE,
    '#required' => FALSE,
    '#parents' => array('extra', 'mappings'),
  );
  $title_prefix = "{$item['store']}: ";
  $fields = array();
  $function_field_template = function($field_name, $value) use ($title_prefix, $option_item_template, $form, $form_components) {
    $default_value = FALSE;
    if (isset($form_state['values']['extra']['mappings'][$field_name])) {
      $default_value = $form_state['values']['extra']['mappings'][$field_name];
    }
    else if (isset($form['extra']['mappings'][$field_name]['#value'])) {
      $default_value = $form['extra']['mappings'][$field_name]['#value'];
    }
    else if (isset($form['extra']['mappings'][$field_name]['#default_value'])) {
      $default_value = $form['extra']['mappings'][$field_name]['#default_value'];
    }
    else if (isset($form['component_extra']['#value']['mappings'][$field_name])) {
      $default_value = $form['component_extra']['#value']['mappings'][$field_name];
    }
    // - TRUE: this field exists and is required (it must have a form field
    //   mapped to it, with info in it when the form is submitted).
    // - string|number: this field exists and has a default value of
    //   VALUE, whatever that is.  E.g. ['amount'] => '100.00'.
    // - FALSE: this field exists and is optional.
    $field = array('#title' => $title_prefix . $field_name) + $option_item_template;
    $field['#parents'][] = $field_name;
    if ($value === TRUE) {
      $field['#required'] = TRUE;
    }
    else if (!empty($value)) {
      // Choices in #options cannot be escaped: doing so leads to double-
      // escaping.
      // @see form_select_options()
      $field['#options']['hardcode'] = t('Item default: "!value"', array('!value' => $value));
      $field['#default_value'] = 'hardcode';
    }
    if (!empty($default_value)) {
      $field['#default_value'] = $default_value;
    }
    $field['#options'] += $form_components;
    return $field;
  };
  // Add the common fields every item has.
  foreach (array('gl', 'amount', 'qty', 'desc') as $common_field) {
    $field_name = 'common: ' . $common_field;
    $fields[$field_name] = $function_field_template($field_name, $item[$common_field]);
  }
  // Add extra information about how the "amount" field works.
  $field_name = 'common: ' . 'amount';
  $fields[$field_name]['#description'] = t('Webform CASHNet will try to turn any referenced field value into a decimal number by applying the REGEXP "<code>.*?([\d.,]*?)[^\d]*$</code>" to find the <strong>last</strong> set of contiguous digits (and separators "." and ",").  For example, "Theatre 101 - $1,075.50 USD" will become "1,075.50", because that is the last set of digits found in the text.');
  // Add any item-specific ref fields.
  foreach ($item['refs'] as $refkey => $value) {
    $field_name = 'ref: ' . $refkey;
    $fields[$field_name] = $function_field_template($field_name, $value);
  }
  // Add the fields every submission has.
  $fields['submission_fieldset'] = array(
    '#title' => t('Per-Submission Fields'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('These fields are shared over the whole submission (no matter how many items are submitted).  Each submitted item will have a chance to set these fields; the last-processed item will therefore have the last word for any given field.'),
  );
  foreach ($item['submission'] as $per_submission_field => $value) {
    $field_name = 'submission: ' . $per_submission_field;
    $fields['submission_fieldset'][$field_name] = $function_field_template($field_name, $value);
  }
  return $fields;
}

/**
 * AJAX callback for the item_code element on _webform_edit_cashnet_item().
 */
function webform_cashnet_ajax_edit_cashnet_item_item_code($form, $form_state) {
  return $form['mappings_fieldset']['mappings'];
}

/**
 * Implements hook_form_FORM_ID_alter() for webform-component-edit-form.
 *
 * Add applicable CASHNet fields/mappings to the form.
 */
function webform_cashnet_form_webform_component_edit_form_alter(&$form, &$form_state, $form_id) {
  // Only alter CASHNet Item forms
  if (empty($form['type']) || empty($form['type']['#value']) || $form['type']['#value'] != 'cashnet_item') {
    return;
  }
  if (!user_access('edit webform cashnet component')) {
    // Enforce access restrictions.
    drupal_access_denied();
    return;
  }
  if (!isset($form['#validate'])) {
    $form['#validate'] = array();
  }
  $form['#validate'][] = 'webform_cashnet_form_webform_component_edit_form_validate';
  $item_code = FALSE;
  if (isset($form_state['values']['extra']['item_code'])) {
    $item_code = $form_state['values']['extra']['item_code'];
  }
  else if (isset($form['extra']['item_code']['#value'])) {
    $item_code = $form['extra']['item_code']['#value'];
  }
  else if (isset($form['extra']['item_code']['#default_value'])) {
    $item_code = $form['extra']['item_code']['#default_value'];
  }
  if (!empty($item_code)) {
    $item = _webform_cashnet_get_items($item_code);
    if (!empty($item)) {
      $fields = _webform_cashnet_get_item_form_fields(current($item), $form, $form_state);
      $form['mappings_fieldset']['mappings'] += $fields;
      unset($form['mappings_fieldset']['mappings']['#markup']);
    }
    else {
      $form['mappings_fieldset']['mappings']['#markup'] = t('<em>No mappings available for this item.</em>');
    }
  }
  else {
    $form['mappings_fieldset']['mappings']['#markup'] = t('<em>Choose a CASHNet item to load the available mappings.</em>');
  }
}

/**
 * Form validation callback from webform_cashnet_form_webform_component_edit_form_alter().
 *
 * Prevent unauthorized submission of edits/clones against CASHNet Items.
 */
function webform_cashnet_form_webform_component_edit_form_validate($form, &$form_state) {
  if (!user_access('edit webform cashnet component')) {
    // Enforce access restrictions here - can't return MENU_ACCESS_DENIED as it
    // won't bubble up to menu_execute_active_handler().
    form_set_error('type', t('Access to the CASHNet Item type has been restricted.  Please contact the site administrator.'));
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for webform-components-form.
 *
 * Provide a layer of access restrictions for adding/editing CASHNet items.
 */
function webform_cashnet_form_webform_components_form_alter(&$form, &$form_state, $form_id) {
  if (!user_access('edit webform cashnet component')) {
    // If the user doesn't have the right permission:
    //  - Add #post_render to set "disabled" on the <option>.
    //  - Add #validate to the form to ensure the CASHNet <option> was not used.
    if (!isset($form['add']['type']['#post_render'])) {
      $form['add']['type']['#post_render'] = array();
    }
    $form['add']['type']['#post_render'][] = 'webform_cashnet_component_disable_option_post_render';

    if (!isset($form['#validate'])) {
      $form['#validate'] = array();
    }
    $form['#validate'][] = 'webform_cashnet_component_disable_option_validate';
    if (!isset($form['add']['add']['#validate'])) {
      $form['add']['add']['#validate'] = array();
    }
    $form['add']['add']['#validate'][] = 'webform_cashnet_component_disable_option_validate';
  }
}

/**
 * Post-render callback from webform_cashnet_form_webform_components_form_alter().
 *
 * Disable the CASHNet Item <option> in the Add Component Type <select>.
 */
function webform_cashnet_component_disable_option_post_render($content, $elements){
  // Disable the cashnet option in the component field type select box.
  $content = preg_replace('~<option([^>]+)value="cashnet_item"~i', '<option\\1disabled="disabled" value="cashnet_item"', $content);
  return $content;
}

/**
 * Form validation callback from webform_cashnet_form_webform_components_form_alter().
 *
 * Prevent unauthorized submission of Add Component form for CASHNet Items.
 */
function webform_cashnet_component_disable_option_validate($form, &$form_state) {
  $selected_type = !empty($form_state['values']['add']['type']) ? strtolower(trim($form_state['values']['add']['type'])) : '';
  if ($selected_type == 'cashnet_item') {
    form_set_error('type', t('Access to the CASHNet Item type has been restricted.  Please contact the site administrator.'));
  }
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * Enforce "edit webform cashnet component" editing permissions on CASHNet Item
 * components in the "webform-components" table.
 */
function webform_cashnet_preprocess_table(&$variables){
  // Only interact with the "webform-components" table.
  $table_id_not_empty = !empty($variables) && !empty($variables['attributes']) && !empty($variables['attributes']['id']);
  $table_id = $table_id_not_empty ? $variables['attributes']['id'] : '';
  if ($table_id != 'webform-components') {
    return;
  }
  // Check user access, and remove links accordingly.
  if (!user_access('edit webform cashnet component')) {
    if (!empty($variables['rows'])) {
      foreach ($variables['rows'] as &$row){
        if (!empty($row['data']) && count($row['data']) > 6) {
          $component_type = strtolower($row['data'][1]);
          if ($component_type == 'cashnet item') {
            // For each CASHNet Item field, remove edit and clone links.
            foreach ($row['data'] as $index => &$col) {
              // Skip Label, Type, and Value columns.
              if ($index > 3) {
                $col = preg_replace('~^\s*<a.*?>(Edit|Clone)</a>\s*$~i', '', $col);
              }
            }
          }
        }
      }
    }
  }
  return $variables;
}

/**
 * Implements hook_form_FORM_ID_alter() for webform-client-form.
 *
 * Add our submission and validation handlers to the form.
 */
function webform_cashnet_form_webform_client_form_alter(&$form, &$form_state, $form_id) {
  // Check if this form has any CASHNet items.
  $has_cashnet_item_component = FALSE;
  if (!empty($form['#node']) && !empty($form['#node']->webform)) {
    foreach ($form['#node']->webform['components'] as $component) {
      if ($component['type'] == 'cashnet_item') {
        $has_cashnet_item_component = TRUE;
        break;
      }
    }
  }
  if (!$has_cashnet_item_component) {
    return;
  }
  $form['#validate'][] = 'webform_cashnet_webform_client_form_validate';
  // Push our submit function first, so that we can update our component's
  // value with the result of our processing.
  // array_unshift($form['#submit'], 'webform_cashnet_webform_client_form_submit');
  // The secondary submit function sets the redirection on the form and needs
  // to run last (at least, after Webform's submission handler).
  $form['#submit'][] = 'webform_cashnet_webform_client_form_submit';
}

/**
 * Validation callback for webform-client-form.
 *
 * Check that all required CASHNet Item fields have data, and compile the actual
 * submission.  Our submit callback will handle the submission.
 */
function webform_cashnet_webform_client_form_validate($form, &$form_state) {
  
}

/**
 * Secondary submission callback for webform-client-form.
 *
 * If we've submitted info, redirect the form.
 */
function webform_cashnet_webform_client_form_submit($form, &$form_state) {
  $components = array();
  if (empty($form_state['webform']) || empty($form_state['webform']['component_tree']) || empty($form_state['webform']['component_tree']['children'])) {
    return;
  }
  // We don't have to worry about $form_state['storage'][X], as the Webform
  // module will restore $form_state['values']['submitted'][X] on multi-page
  // forms.  Instead, just check if the form is complete.
  if (empty($form_state['webform_completed'])) {
    return;
  }
  $is_new = !empty($form_state['values']['details']['is_new']);
  $is_draft = !empty($form_state['save_draft']);
  // Don't resubmit updates to existing entries, or draft submissions.
  if (!$is_new || $is_draft) {
    return;
  }
  // Compile a list of any CASHNet items on the form.
  foreach ($form_state['webform']['component_tree']['children'] as $component) {
    if (!empty($component) && !empty($component['type']) && $component['type'] == 'cashnet_item') {
      $components[$component['form_key']] = $component;
    }
  }
  // If there are no CASHNet items in the form, stop processing.  This is really
  // an error condition and should never happen (since this validation function
  // is only invoked when a CASHNet item is on a form, in theory).
  if (empty($components)) {
    return;
  }
  // CASHNet does not allow some characters/scenarios.
  // If you pass a "<" character, the visitor will receive this error:
  //   The CASHNet page you are trying to reach does not exist or an error has
  //   occurred.
  // If you pass the "--" sequence, the visitor will see the message:
  //   Your Request Has Been Blocked
  //   This can happen if you entered unexpected characters. 
  // Allowable characters include: `~!@#$%^&*()-_+={[}]|\;:'",.>/?
  // HTML entity sequences are weird as well: The sequence "&#" followed by any
  // number (e.g. "&#0") will result in the "blocked request" message, but "&#"
  // followed by any letter (e.g. "&#a") will result in the "page does not exist
  // or an error has occurred" error message.
  // We'll work around these limitations during the value assignment phase,
  // near the end of each CASHNet component's loop, below.
  // CASHNet ostensibly supports UTF-8 but will ultimately transliterate down
  // to a common Western (ISO-8859-1 or CP1252-like) subset. We'll send UTF-8
  // sequences just in case support improves in the future.
  $unicode_replacement = chr(0xEF) . chr(0xBF) . chr(0xBD);
  $bad_chars_search = array('<', '--', '&#');
  // Make the sequences valid in a reversible manner so that any post-processing
  // of values can undo this step after CASHNet processing is done.
  $bad_chars_replace = array($unicode_replacement, "-{$unicode_replacement}", "&{$unicode_replacement}#");
  // Build the items for submission. Note: there may be multiple of any given
  // item, if the controlling form fields have been submitted multiple times.
  $item_definitions = _webform_cashnet_get_items();
  $items = array();
  $per_submission_values = _webform_cashnet_per_submission_values();
  foreach ($components as $form_key => $component) {
    $errors = FALSE;
    if (empty($component['extra']['item_code'])) {
      continue;
    }
    // Check this item's submission control before continuing.
    if (!empty($component['extra']['control_field_name'])) {
      // By default, submit the form, unless some control element says not to.
      $control_submit = TRUE;
      $control_field = $component['extra']['control_field_name'];
      $control_values = trim($component['extra']['control_field_value']);
      $control_field_empty = empty($form_state['values']['submitted'][$control_field]);
      $control_field_isvalue = preg_match(
        '~^\s*' . preg_quote(trim($form_state['values']['submitted'][$control_field]), '~') . '\s*$~mi',
        $control_values
      ) === 1;
      switch($component['extra']['control_field_type']) {
        case 'field_empty':
          if (!$control_field_empty) {
            $control_submit = FALSE;
          }
          break;
        case 'field_notempty':
          if ($control_field_empty) {
            $control_submit = FALSE;
          }
          break;
        case 'field_value':
          if (!$control_field_isvalue) {
            $control_submit = FALSE;
          }
          break;
        case 'field_notvalue':
          if ($control_field_isvalue) {
            $control_submit = FALSE;
          }
          break;
        default:
          break;
      }
      // If processing of the control statement resulted in a "DO NOT SUBMIT",
      // then skip this item.
      if (!$control_submit) {
        continue;
      }
    }
    // Make a copy of the item's definition; we'll fill in the copy's values.
    $item = $item_definitions[$component['extra']['item_code']];
    $item['itemcode'] = $component['extra']['item_code'];
    // Remove the common per-submission stuff, since that won't be handled
    // at the per-item level.
    unset($item['submission']);
    // Process every referenced value for the current CASHNet form component.
    foreach ($component['extra']['mappings'] as $key_composite => $mapping) {
      list($field_type, $field_name) = explode(': ', $key_composite, 2);
      $mapping = explode(': ', $mapping, 2);
      $mapping[] = FALSE;
      list($source_type, $source_value) = $mapping;
      // $field_type is either "common", "submission", or "ref".
      // $source_type is either "hardcode", "component", or an empty string.
      if ($field_type == 'ref') {
        // "ref" types are stored under the ref key.
        $item_field_pointer = &$item['refs'][$field_name];
      }
      else if ($field_type == 'submission') {
        // "submission" types need to be stored in the per-submission array.
        $item_field_pointer = &$per_submission_values[$field_name];
      }
      else {
        // "common"-type values are stored in the item root.
        $item_field_pointer = &$item[$field_name];
      }
      // If $source_type is "hardcode", we don't need to do anything, as the
      // hardcoded default value is already in the item since we copied it from
      // the definition.
      if ($source_type == 'component') {
        // Webform's submission callback (via flatten_form) replaces the key in
        // $form_state['values']['submitted'] with the CID, which is what our
        // config form used as well, so reference it directly and we're done!
        if (isset($form_state['values']['submitted'][$source_value])) {
          $source_component = $form['#node']->webform['components'][$source_value];
          // Use "CSV" to determine whether or not this component can be used for
          // CASHNet (kind of similar - submitting to a flat, text-only 3rd party).
          if (webform_component_feature($source_component['type'], 'csv')) {
            // Start by assigning the basic string value of a component's value.
            $item_field_pointer = strval($form_state['values']['submitted'][$source_value]);
            // Attempt to get the component's preferred rendered text.
            // $display = webform_component_invoke($source_component['type'], 'display', $source_component, (array) $form_state['values']['submitted'][$source_value], 'text');
            $display = webform_component_invoke($source_component['type'], 'csv_data', $source_component, NULL, (array) $form_state['values']['submitted'][$source_value]);
            if (!empty($display)) {
              $item_field_pointer = $display;
            }
            // Fix any illegal (in CASHNet) character sequences.
            $item_field_pointer = str_replace($bad_chars_search, $bad_chars_replace, $item_field_pointer);
          }
        }
        else if ($item_field_pointer !== TRUE) {
          $item_field_pointer = '';
        }
      }
      // Check if the fields's definition marks it as required (TRUE), and we
      // haven't set any data on it (it's still TRUE).
      if ($item_field_pointer === TRUE) {
        watchdog('webform_cashnet', t('Webform @nid CASHNet item @item_form_key is missing required value @key_composite.'), array('@nid' => $form['#node']->nid, '@item_form_key' => $component['form_key'], '@key_composite' => $key_composite), WATCHDOG_ERROR);
        $errors = TRUE;
      }
      // Clean up the pointer for the next loop iteration.
      unset($item_field_pointer);
    }
    if (!$errors) {
      $items[] = $item;
    }
  }
  drupal_alter('webform_cashnet_presubmit_items', $items);
  drupal_alter('webform_cashnet_presubmit_shared_values', $per_submission_values);
  // Remove empty submission values.
  foreach ($per_submission_values as $key => $value) {
    if (empty($value)) {
      unset($per_submission_values[$key]);
    }
  }
  // Compile the actual submission.
  $submission = array('virtual' => FALSE) + $per_submission_values;
  $submission['itemcnt'] = count($items);
  // Only submit to CASHNet if we have at least one item to submit.
  if ($submission['itemcnt'] <= 0) {
    return;
  }
  $item_index = 1;
  $per_submission_ref_values = _webform_cashnet_per_submission_reference_values();
  foreach ($items as $item) {
    // Call this item's "submission callback" if any is defined.
    if (!empty($item['submission callback'])) {
      $function = trim($item['submission callback']);
      if (is_callable($function)) {
        $function($form_state, $item);
      }
    }
    // Set the submission store to the first store we have defined.
    if (!empty($item['store']) && empty($submission['virtual'])) {
      $submission['virtual'] = $item['store'];
    }
    // Process the common fields every item has.
    foreach (array('itemcode', 'desc', 'qty', 'gl') as $common_value) {
      if (!empty($item[$common_value])) {
        if ($common_value === 'qty') {
          // Limit $qty to integers within [1, +inf].
          $item[$common_value] = max(1, intval($item[$common_value]));
        }
        $submission[$common_value . $item_index] = $item[$common_value];
      }
    }
    if (!empty($item['amount'])) {
      // The given REGEXP pattern will find the last of any numeric strings
      // (separated by "." or "," characters), and floatval() them.
      // Example: "XYZ 123 - $2.00" will become "2.0".
      // Example: "XYZ 123 - $2.00 ABC" will become "2.0".
      // Example: "123 456" will become "456.0".
      // Example: "123 456 ABC" will become "456.0".
      $amount = floatval(preg_replace('/.*?([\d.,]*?)[^\d]*$/', '\\1', $item['amount']));
      // Do any quantity math.
      if (!empty($item['qty'])) {
        $amount = $amount * $item['qty'];
      }
      $submission['amount' . $item_index] = $amount;
    }
    // Add any reference fields.
    $ref_index = 1;
    foreach ($item['refs'] as $reftype => $refval) {
      if (!empty($refval)) {
        $reftype = strtoupper($reftype);
        // Check if this reference value's reftype is a special per-submission
        // type, and if so, make sure it is only added once. CASHNet will only
        // use the first value submitted, so we'll just skip subsequent values.
        // @see _webform_cashnet_per_submission_reference_values()
        if (empty($per_submission_ref_values[$reftype])) {
          // Either:
          //  - This is NOT a special once-per-submission reftype, or
          //  - This IS a special once-per-submission reftype but it hasn't been
          //    submitted yet.
          $submission['ref' . $ref_index . 'type' . $item_index] = $reftype;
          $submission['ref' . $ref_index . 'val' . $item_index] = $refval;
          if (isset($per_submission_ref_values[$reftype])) {
            $per_submission_ref_values[$reftype] = TRUE;
          }
          ++$ref_index;
        }
      }
    }
    ++$item_index;
  }
  drupal_alter('webform_cashnet_presubmit_submission', $submission);
  $cashnet_endpoint = variable_get('webform_cashnet_endpoint', 'https://commerce.cashnet.com/404Handler/pageredirget.aspx');
  $query_string = drupal_http_build_query($submission);
  $destination = $cashnet_endpoint . '?' . $query_string;
  // We need to enforce our redirection.
  if (isset($_GET['destination'])) {
    unset($_GET['destination']);
  }
  $form_state['no_redirect'] = FALSE;
  $form_state['redirect'] = array($destination, array('absolute' => TRUE, 'external' => TRUE, 'https' => TRUE), 302);
}
